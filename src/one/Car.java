package one;

public class Car extends Vehicle {
	
	
	//overload constructor
	public Car(String name_brand){
		super(name_brand);
	}
	
	@Override
	public String getTires() {
		return "4 tires";
	}
		
}
