package one;

public class Motorcycle extends Vehicle{
	//overload constructor
	public Motorcycle(String name_brand){
		super(name_brand);
		
	}

	@Override
	public String getTires() {
		return "2 Tires";
	}

}
