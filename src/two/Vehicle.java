package two;

public abstract class Vehicle {
	private String brandName;
	
	public Vehicle(){
	}
	
	public abstract String Tires();
	
	public Vehicle(String brand_name){
		this.brandName = brand_name;
	}
	
}
