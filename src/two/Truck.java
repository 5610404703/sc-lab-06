package two;

public class Truck extends Vehicle{
	public Truck(){
		super();
	}
	
	//overload constructor
	public Truck(String name_brand){
		super(name_brand);
	}
	
	public int Seat(int seat){
		return seat;
	}
	
	public String Seat(String seat){//overloading method
		return seat;
	}

	@Override
	public String Tires() {
		return "10 Tires";
	}
	
	

}
