package two;


public class Motorcycle extends Vehicle{
	
	public Motorcycle(){
		super();
	}
	
	//overload constructor
	public Motorcycle(String name_brand){
		super(name_brand);
	}
	
	@Override
	public String Tires() {
		return "2 tires";
	}

}
