package two;


public class Car extends Vehicle {
	public Car(){
		super();
	}
	
	//overload constructor
	public Car(String name_brand){
		super(name_brand);
	}

	@Override
	public String Tires() {
		return "4 tires";
	}
		
}
